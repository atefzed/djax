# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Person(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	photo = models.FileField(upload_to='photos')
	owner = models.ForeignKey(User,null=True, blank=True)