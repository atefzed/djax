# -*- coding: utf-8 -*-
from django import forms

class PersonForm(forms.Form):
	first_name = forms.CharField(max_length=50, required =True)
	last_name = forms.CharField(max_length=50, required =True)
	photo  = forms.forms.FileField(
		        label='Select a photo',
		        help_text='max. 42 megabytes',
		        required =True
    			)