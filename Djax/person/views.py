# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, render, redirect
from django.template import RequestContext
from django.http import HttpResponse,HttpResponseRedirect
from django.core.urlresolvers import reverse
from person.models import Person
from person.forms import PersonForm
from django.contrib.auth.models import User
from django.utils import simplejson as json
from django.core import serializers
import logging

logger = logging.getLogger(__name__)

def index(request):

	form = PersonForm()
	persons = Person.objects.all()

	return render_to_response('index.html', {'persons': persons, 'form': form},
        						context_instance=RequestContext(request))


def add(request):
	if request.method == 'POST' :
		form = PersonForm(request.POST, request.FILES)
        # u = User.objects.get(pk =request.user)
		if form.is_valid():
			newper = Person(
				first_name = request.POST['first_name'],
				last_name = request.POST['last_name'],
				photo = request.FILES['photo'],
				owner = request.user
		    	)
			newper.save()

	return redirect('/')

def update(request):
	if request.method == 'POST' :
		form = PersonForm(request.POST, request.FILES)
		pid = request.POST['perid']
		per = Person.objects.get(pk = int(pid))
		per.first_name = request.POST['first_name']
		per.last_name = request.POST['last_name']
		if 'photo' in request.FILES:
			per.photo = request.FILES['photo']
		per.save()

	return redirect('/')

def delete(request, pid):
	if request.method == 'POST' and request.is_ajax():
		p = Person.objects.get(pk= pid)
		p.delete()
	
	return HttpResponse(status=200)