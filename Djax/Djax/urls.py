# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'person.views.index'),
	url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'static'}),
    url(r'^add/', 'person.views.add'),
    url(r'^delete/(?P<pid>\w+)', 'person.views.delete', name='person_delete'),
    url(r'^update/', 'person.views.update'),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)